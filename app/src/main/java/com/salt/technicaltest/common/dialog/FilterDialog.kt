package com.salt.technicaltest.common.dialog

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.salt.technicaltest.R
import com.salt.technicaltest.common.adapter.FilterCategoryAdapter
import com.salt.technicaltest.common.adapter.FilterCountryAdapter
import com.salt.technicaltest.databinding.DialogFilterBinding
import com.salt.technicaltest.datasource.HeadlineFilter
import com.salt.technicaltest.utils.zeecommon.ui.AppDialogFragment
import com.salt.technicaltest.utils.zeecommon.ui.HasObservers
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FilterDialog(
    private val mOnResultBlock: (data: HeadlineFilter) -> Unit
) : AppDialogFragment<DialogFilterBinding, FilterDialogViewModel>(R.layout.dialog_filter),
    HasObservers {

    override val viewModel by viewModels<FilterDialogViewModel>()
    override val dialogWindowFullscreen: Boolean = true

    private val mCountryAdapter by lazy { FilterCountryAdapter(viewModel) }

    private val mCategoryAdapter by lazy { FilterCategoryAdapter(viewModel) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setup()

        viewBinding.rvCountry.adapter = mCountryAdapter
        viewBinding.rvCategory.adapter = mCategoryAdapter
    }

    override fun setupObservers() {
        viewModel.onResultShouldPassedEvent.observe(viewLifecycleOwner) {
            dismiss()
            mOnResultBlock(it)
        }
        viewModel.refreshAdapterEvent.observe(viewLifecycleOwner) {
            when(it){
                0 -> mCountryAdapter.notifyDataSetChanged()
                else -> mCategoryAdapter.notifyDataSetChanged()
            }
        }
    }

}