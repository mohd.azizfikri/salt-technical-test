package com.salt.technicaltest.common.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.salt.technicaltest.databinding.ItemFilterBinding
import com.salt.technicaltest.datasource.Item
import com.salt.technicaltest.utils.zeecommon.util.list.AppRecyclerView

class FilterCategoryAdapter (
    private val mListener: itemAdapterListener
) : AppRecyclerView<ItemFilterBinding, Item>(Item.DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemFilterBinding = ItemFilterBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemFilterBinding,
        model: Item
    ) {
        binding.data = model

        binding.llItem.setOnClickListener {
            mListener.onClickCategoryItem(model)
        }
    }

    interface itemAdapterListener {
        fun onClickCategoryItem(data: Item)
    }

}