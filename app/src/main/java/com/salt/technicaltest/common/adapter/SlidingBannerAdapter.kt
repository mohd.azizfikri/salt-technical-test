package com.salt.technicaltest.common.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class SlidingBannerAdapter(protected val items: MutableList<Item> = mutableListOf()) :
    RecyclerView.Adapter<SlidingBannerAdapter.ViewHolder>() {

    abstract fun onCreateView(parent: ViewGroup, viewType: Int): View

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(onCreateView(parent, viewType))
    }

    override fun getItemCount(): Int = items.size

    fun submitList(newItems: List<Item>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    open class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    open class Item(val identifier: String) {
        companion object {
            val DIFF_UTIL = object : DiffUtil.ItemCallback<Item>() {
                override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                    return oldItem == newItem
                }

                override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                    return oldItem.identifier == newItem.identifier
                }
            }
        }
    }
}