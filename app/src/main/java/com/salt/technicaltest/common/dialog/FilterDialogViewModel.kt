package com.salt.technicaltest.common.dialog

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.natpryce.Success
import com.salt.technicaltest.common.adapter.FilterCategoryAdapter
import com.salt.technicaltest.common.adapter.FilterCountryAdapter
import com.salt.technicaltest.datasource.HeadlineFilter
import com.salt.technicaltest.datasource.Item
import com.salt.technicaltest.utils.listCategory
import com.salt.technicaltest.utils.listCountry
import com.salt.technicaltest.utils.zeecommon.extension.mutateList
import com.salt.technicaltest.utils.zeecommon.ui.AppViewModel
import com.salt.technicaltest.utils.zeecommon.util.event.LiveEvent
import com.salt.technicaltest.utils.zeecommon.util.event.MutableLiveEvent
import com.salt.technicaltest.utils.zeecommon.util.list.AppDataWrapper
import kotlinx.coroutines.launch

class FilterDialogViewModel  @ViewModelInject constructor() :
    AppViewModel(),
    FilterCountryAdapter.itemAdapterListener,
    FilterCategoryAdapter.itemAdapterListener {

    private val _onResultShouldPassedEvent by lazy { MutableLiveEvent<HeadlineFilter>() }
    val onResultShouldPassedEvent: LiveEvent<HeadlineFilter> = _onResultShouldPassedEvent

    private val _refreshAdapterEvent by lazy { MutableLiveEvent<Int>() }
    val refreshAdapterEvent: LiveEvent<Int> = _refreshAdapterEvent

    val countryWrapper by lazy { AppDataWrapper<Item>() }
    val categoryWrapper by lazy { AppDataWrapper<Item>() }

    val searchText by lazy { MutableLiveData<String>() }
    val country by lazy { MutableLiveData<String>() }
    val category by lazy { MutableLiveData<String>() }
    val sources by lazy { MutableLiveData<String>() }
    val pageSize by lazy { MutableLiveData<String>() }
    val page by lazy { MutableLiveData<String>() }

    fun setup() {
        viewModelScope.launch {
            countryWrapper.load { Success(listCountry()) }
            categoryWrapper.load { Success(listCategory()) }
        }
    }

    fun clickFilter(){
        _onResultShouldPassedEvent.set(
            HeadlineFilter(
                keyword = searchText.value ?: "",
                country = country.value ?: "id",
                category = category.value ?: "",
            )
        )
    }

    override fun onClickCountryItem(data: Item) {
        country.value = data.id

        countryWrapper.liveData.value = countryWrapper.liveData.value?.map { it.selected = false; it }
        data.selected = !data.selected
        countryWrapper.liveData.mutateList(data, {item -> data.id == item.id})

        _refreshAdapterEvent.set(0)
    }

    override fun onClickCategoryItem(data: Item) {
        category.value = data.id

        categoryWrapper.liveData.value = categoryWrapper.liveData.value?.map { it.selected = false; it }
        data.selected = !data.selected
        categoryWrapper.liveData.mutateList(data, {item -> data.id == item.id})

        _refreshAdapterEvent.set(1)
    }
}