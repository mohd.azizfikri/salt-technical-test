package com.salt.technicaltest.common.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.salt.technicaltest.databinding.ItemListArticleBinding
import com.salt.technicaltest.utils.zeecommon.SelectableData
import com.salt.technicaltest.utils.zeecommon.util.list.AppRecyclerView
import com.salt.technicaltest.utils.zeedata.api.response.itemresponse.NewsResponse

class ArticleListAdapter(
    private val mListener: ArticleListener
) : AppRecyclerView<ItemListArticleBinding, SelectableData<NewsResponse>>(DIFF_UTIL) {

    override fun onCreateViewBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ItemListArticleBinding = ItemListArticleBinding.inflate(inflater, parent, false)

    override fun onPrepareBindViewHolder(
        binding: ItemListArticleBinding,
        model: SelectableData<NewsResponse>
    ) {
        binding.data = model.origin

        binding.llArticle.setOnClickListener {
            mListener.onClickArticle(model.origin)
        }
    }

    interface ArticleListener {
        fun onClickArticle(data: NewsResponse)
    }

    companion object {

        val DIFF_UTIL = object : DiffUtil.ItemCallback<SelectableData<NewsResponse>>() {

            override fun areItemsTheSame(
                oldItem: SelectableData<NewsResponse>,
                newItem: SelectableData<NewsResponse>
            ): Boolean {
                return oldItem.origin == newItem.origin
            }

            override fun areContentsTheSame(
                oldItem: SelectableData<NewsResponse>,
                newItem: SelectableData<NewsResponse>
            ): Boolean {
                return oldItem.origin.url == newItem.origin.url
            }

        }
    }

}