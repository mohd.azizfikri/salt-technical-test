package com.salt.technicaltest

import android.app.Application
import com.salt.technicaltest.utils.Util
import com.salt.technicaltest.utils.zeecommon.CommonProvider
import com.salt.technicaltest.utils.zeedata.DataConfiguration
import com.salt.technicaltest.utils.zeedata.DataProvider
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoreApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        Util.init(this)
        CommonProvider.init(BR.viewmodel)
        DataProvider.init(
            this, DataConfiguration(
                remoteHost = "https://newsapi.org/",
                remoteTimeout = 60L,
                debug = BuildConfig.DEBUG
            )
        )
    }
}