package com.salt.technicaltest.utils.zeedata.api.response.itemresponse

data class RegisterResponse(
    val email: List<String>? = null,
    val name: List<String>? = null,
    val password: List<String>? = null,
    val password_confirmation: List<String>? = null,
    val username: List<String>? = null,
    val nextUrl: String
)