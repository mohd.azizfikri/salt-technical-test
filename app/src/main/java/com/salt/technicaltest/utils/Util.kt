package com.salt.technicaltest.utils

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.Spanned
import com.salt.technicaltest.utils.zeecommon.extension.toMoneyFormat
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

object Util {

    private var mContext: Context? = null

    fun init(context: Context) {
        mContext = context
    }

    @JvmStatic
    fun changeFromRp(bsa: String): String{
        return if(bsa.contains("rp",true)){
            bsa.removePrefix("Rp ").replace(".", "")
        }else{
            bsa
        }
    }

    @JvmStatic
    fun changeToRp(bsa: String): String{
        return if(!bsa.contains("rp",true)){
            bsa.toDoubleOrNull()!!.toMoneyFormat(Constant.PREFIX_RP)
        }else{
            bsa
        }
    }

    @JvmStatic
    fun implementHtml(bsa: String): Spanned{
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(bsa, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(bsa)
        }
    }

    @JvmStatic
    fun changeDateFormat(source: String?, oldFormat: String): String{
        return if(source.isNullOrEmpty()){
            ""
        }else {
            val sdf = SimpleDateFormat(oldFormat)
            val d: Date = sdf.parse(source)
            sdf.applyPattern("dd MMM yyyy")
            sdf.format(d)
        }

    }
    @JvmStatic
    fun changeDateFormat(source: String?): String{
        return if(source.isNullOrEmpty()){
            ""
        }else {
            return try{
                val format = SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US
                )

                val d: Date = format.parse(source)
                format.applyPattern("dd MMM yyyy")
                format.format(d)

            }catch (e: Exception){
                source
            }
        }
    }

    @JvmStatic
    fun firstNameOf(fullname: String?): String{
        return if(!fullname.isNullOrEmpty()) {
            if (fullname.contains(" ")) fullname.split(" ")[0] else fullname
        }else{
            ""
        }
    }
}