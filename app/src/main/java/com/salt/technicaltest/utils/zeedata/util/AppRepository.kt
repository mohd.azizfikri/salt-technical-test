package com.salt.technicaltest.utils.zeedata.util

import com.google.gson.Gson
import com.natpryce.Failure
import com.natpryce.Result
import com.natpryce.Success
import com.natpryce.onFailure
import com.salt.technicaltest.utils.zeedata.DataProvider
import com.salt.technicaltest.utils.zeedata.api.response.general.DataListResponse
import com.salt.technicaltest.utils.zeedata.api.response.general.DataResponse
import com.salt.technicaltest.utils.zeedata.api.response.general.MessageResponse
import com.salt.technicaltest.utils.zeedata.exception.FailureRemoteResultException
import com.salt.technicaltest.utils.zeedata.exception.IllegalRemoteResultException
import com.salt.technicaltest.utils.zeedata.exception.UnexpectedHttpResultException
import com.salt.technicaltest.utils.zeedata.exception.UnexpectedRepositoryResultException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.HttpException
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

abstract class AppRepository : CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.IO

    protected fun getApi() = DataProvider.getApiService()

    protected fun getAppDb() = DataProvider.getAppDatabase()

    /**
     * Taking result as list:
     * Will not check status == false or not, cause it doesn't understand the response base structure.
     */
    inline fun <T> asDirectResult(block: () -> Response<T>): Result<T, Exception> {
        try {
            val response = block()
            if (response.isSuccessful) {
                response.body()?.let { body -> return Success(body) }
                // if body null, just pass it, return as illegal result (below)
            }
            return Failure(IllegalRemoteResultException(response.message()))
        } catch (e: HttpException) {
            val errorBody = e.response()?.errorBody()?.string()
            val response = Gson().fromJson(errorBody, MessageResponse::class.java)
            Failure(UnexpectedHttpResultException(response))
        } catch (e: Exception) {
            return Failure(e)
        }
        return Failure(UnexpectedRepositoryResultException())
    }

    /**
     * Taking result as list:
     * If status == false, then return FailureRemoteResultException(message) automatically
     */
    inline fun <A, T : DataListResponse<A>> asResultList(block: () -> Response<DataListResponse<A>>): Result<List<A>, Exception> {
        try {
            val response = block()
            if (response.isSuccessful) {
                val body = response.body()
                // if it's a list, it doesn't need to check if status 1 or 0.
                // if it's 0, then it default should be empty from API.
                return Success(body?.data.orEmpty())
            }
            return Failure(IllegalRemoteResultException(response.message()))
        } catch (e: HttpException) {
            val errorBody = e.response()?.errorBody()?.string()
            val response = Gson().fromJson(errorBody, MessageResponse::class.java)
            Failure(UnexpectedHttpResultException(response))
        } catch (e: Exception) {
            return Failure(e)
        }
        return Failure(UnexpectedRepositoryResultException())
    }

    /**
     * Taking result as single object
     * If status == false, then return FailureRemoteResultException(message) automatically
     */
    inline fun <A, T : DataResponse<A>> asResult(block: () -> Response<DataResponse<A>>): Result<A, Exception> {
        try {
            val response = block()
            if (response.isSuccessful) {
                val body = response.body()
                if (response.code() == 200 && body?.data != null) return Success(body.data)
                return Failure(FailureRemoteResultException(body?.statusDesc.orEmpty()))
            }
            return Failure(IllegalRemoteResultException(response.message()))
        } catch (e: HttpException) {
            val errorBody = e.response()?.errorBody()?.string()
            val response = Gson().fromJson(errorBody, MessageResponse::class.java)
            Failure(UnexpectedHttpResultException(response))
        } catch (e: Exception) {
            return Failure(e)
        }
        return Failure(UnexpectedRepositoryResultException())
    }

    /**
     * Taking result as a message from DataResponse<A>
     * If status == false, then return FailureRemoteResultException(message) automatically
     */
    inline fun <A, T : DataResponse<A>> asResultDataMessage(block: () -> Response<DataResponse<A>>): Result<String, Exception> {
        val result = asDirectResult(block).onFailure { return it }
        return Success(result.statusDesc)
    }

    /**
     * If status == false, then return FailureRemoteResultException(message) automatically
     */
    inline fun asResultMessage(block: () -> Response<MessageResponse>): Result<String, Exception> {
        val result = asDirectResult(block).onFailure { return it }
        if (result.status != 1) return Failure(FailureRemoteResultException(result.message))
        return Success(result.message)
    }


}