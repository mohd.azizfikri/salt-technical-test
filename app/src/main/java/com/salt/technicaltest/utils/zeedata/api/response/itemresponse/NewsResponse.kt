package com.salt.technicaltest.utils.zeedata.api.response.itemresponse

import com.salt.technicaltest.utils.zeedata.api.response.itemresponse.part.Source

data class NewsResponse(
    val author: String,
    val content: String,
    val description: String,
    val publishedAt: String,
    val source: Source,
    val title: String,
    val url: String,
    val urlToImage: String
)