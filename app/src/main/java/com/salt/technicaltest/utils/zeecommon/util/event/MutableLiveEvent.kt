package com.salt.technicaltest.utils.zeecommon.util.event

class MutableLiveEvent<T> : LiveEvent<T>() {

    var value
        get() = action.value
        set(newValue) {
            action.postValue(newValue)
        }

    fun set(value: T) {
        this.value = ActionEvent(value)
    }

}