package com.salt.technicaltest.utils.zeecommon.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.salt.technicaltest.utils.zeecommon.CommonProvider
import com.salt.technicaltest.utils.zeecommon.extension.applyIf
import com.salt.technicaltest.utils.zeecommon.extension.toast

abstract class AppFragment<out T : ViewDataBinding, out V : AppViewModel>(resource: Int) :
    Fragment() {

    protected abstract val viewModel: V

    protected val viewBinding: T by lazy {
        DataBindingUtil.inflate(layoutInflater, resource, null, false)
    }

    protected open var confirmationPositiveButtonText: String = "OK"
    protected open var confirmationNegativeButtonText: String = "Cancel"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.lifecycleOwner = this

        // bindings
        if (this is HasViewModel) viewBinding.setVariable(brViewModelId, viewModel)
        else { CommonProvider.getViewModelBindingId()?.let { id -> viewBinding.setVariable(id, viewModel) } }

        if (this is HasBindings) setupBinding()
        if (this is HasViewModel || this is HasBindings) viewBinding.executePendingBindings()

        // setup / preparation related
        if (this is HasViews) setupViews()
        if (this is HasObservers) setupObservers()

        setupDefaultObserver()
    }

    private fun setupDefaultObserver() {
        viewModel.showToastMessageEvent.observe(this) { requireActivity().toast(it) }
        viewModel.showConfirmationMessageEvent.observe(this) {
            showAlertDialog(it.first, true, it.second)
        }
        viewModel.showAlertMessageEvent.observe(this) {
            showAlertDialog(it.first, false, it.second)
        }
    }

    private fun showAlertDialog(
        message: String,
        cancelable: Boolean = true,
        onConfirm: () -> Unit
    ) {
        AlertDialog
            .Builder(requireActivity())
            .setCancelable(false)
            .setMessage(message)
            .setPositiveButton(confirmationPositiveButtonText) { dialog, _ ->
                onConfirm()
                dialog.dismiss()
            }
            .applyIf(cancelable) {
                setNegativeButton(confirmationNegativeButtonText) { dialog, _ -> dialog.cancel() }
            }
            .show()
    }

}