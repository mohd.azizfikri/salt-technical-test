package com.salt.technicaltest.utils.zeecommon.ui

interface HasParentViewModel {

    val parentViewModel: AppViewModel

}