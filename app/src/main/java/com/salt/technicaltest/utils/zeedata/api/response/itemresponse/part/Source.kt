package com.salt.technicaltest.utils.zeedata.api.response.itemresponse.part

data class Source(
    val id: String,
    val name: String
)