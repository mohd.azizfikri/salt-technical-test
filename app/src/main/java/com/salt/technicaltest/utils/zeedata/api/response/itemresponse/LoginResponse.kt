package com.salt.technicaltest.utils.zeedata.api.response.itemresponse

data class LoginResponse(
    val token: String
)