package com.salt.technicaltest.utils.zeedata.api.request

class PengajuanKPRRequest(
    val area: String,
    val alamat: String,
    val luas_bangunan: String,
    val deskripsi: String,
    val jangka_waktu: String,
    val nama: String,
    val no_telp: String,
    val email: String,
    val jadwal_survey: String,
    val kode_referal: String
)