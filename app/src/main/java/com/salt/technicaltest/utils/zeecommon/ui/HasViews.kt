package com.salt.technicaltest.utils.zeecommon.ui

interface HasViews {

    fun setupViews()

}