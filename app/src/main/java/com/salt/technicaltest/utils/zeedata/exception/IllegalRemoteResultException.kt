package com.salt.technicaltest.utils.zeedata.exception

class IllegalRemoteResultException(message: String) : Exception(message)