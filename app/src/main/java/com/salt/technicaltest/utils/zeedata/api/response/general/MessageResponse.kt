package com.salt.technicaltest.utils.zeedata.api.response.general

import com.google.gson.annotations.SerializedName

/**
 * Data of object
 */
data class MessageResponse(
    @field:SerializedName("status") val status: Int = 0,
    @field:SerializedName("message") val message: String = "",
    @field:SerializedName("error") val error: String? = null
)