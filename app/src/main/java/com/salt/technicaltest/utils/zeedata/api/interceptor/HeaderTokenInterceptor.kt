package com.salt.technicaltest.utils.zeedata.api.interceptor

import com.salt.technicaltest.utils.zeedata.db.AppDatabase
import com.salt.technicaltest.utils.zeedata.db.entity.Configuration
import okhttp3.Request

class HeaderTokenInterceptor() : AdditionalHeaderInterceptor() {

    override fun Request.Builder.onRequestBuilding() {
       addHeader(HEADER_API_KEY, "28736c61e4a247588ad361d79fde89fa")
    }

    companion object {
        private const val HEADER_API_KEY = "X-Api-Key"
    }

}