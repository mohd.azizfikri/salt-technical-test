package com.salt.technicaltest.utils.zeecommon.ui

interface HasViewModel {

    val brViewModelId: Int

}