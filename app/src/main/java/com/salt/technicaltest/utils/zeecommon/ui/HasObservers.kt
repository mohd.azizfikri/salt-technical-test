package com.salt.technicaltest.utils.zeecommon.ui

interface HasObservers {

    fun setupObservers()

}