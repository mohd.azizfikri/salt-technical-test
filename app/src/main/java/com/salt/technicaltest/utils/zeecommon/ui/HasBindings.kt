package com.salt.technicaltest.utils.zeecommon.ui

interface HasBindings {

    fun setupBinding()

}