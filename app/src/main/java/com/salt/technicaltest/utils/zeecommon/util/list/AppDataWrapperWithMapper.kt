package com.salt.technicaltest.utils.zeecommon.util.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.natpryce.Result
import com.natpryce.onFailure

class AppDataWrapperWithMapper<T, E>(mapper: (source: T) -> E) {

    val sourceLiveData by lazy { MutableLiveData(listOf<T>()) }
    val liveData by lazy { Transformations.map(sourceLiveData) { it.map(mapper) } }

    val isEmpty by lazy { Transformations.map(liveData) { it.isEmpty() } }
    val isLoading by lazy { MutableLiveData(false) }

    suspend fun load(enableLoading: Boolean = true, block: suspend () -> Result<List<T>, Exception>) {
        if (enableLoading) isLoading.value = true
        sourceLiveData.value = block().onFailure {
            if (enableLoading) isLoading.value = false
            return
        }
        if (enableLoading) isLoading.value = false
    }
}