package com.salt.technicaltest.utils.zeecommon

data class SelectableData<T>(
    var selected: Boolean = false,
    val origin: T
)