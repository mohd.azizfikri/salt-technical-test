package com.salt.technicaltest.utils.zeedata.exception

import com.salt.technicaltest.utils.zeedata.api.response.general.MessageResponse

class UnexpectedHttpResultException(errorResponse: MessageResponse) :
    Exception(errorResponse.message ?: errorResponse.error)