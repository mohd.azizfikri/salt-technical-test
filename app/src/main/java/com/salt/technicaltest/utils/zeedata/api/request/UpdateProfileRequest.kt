package com.salt.technicaltest.utils.zeedata.api.request

import com.google.gson.annotations.SerializedName
import com.salt.technicaltest.utils.zeedata.api.request.part.PartRequest
import okhttp3.MultipartBody
import java.io.File

class UpdateProfileRequest(
    val name: String = "",
    val email: String = "",
    val telephone: String = "",
    val image: File? = null
)