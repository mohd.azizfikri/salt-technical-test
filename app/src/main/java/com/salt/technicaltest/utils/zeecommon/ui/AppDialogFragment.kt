package com.salt.technicaltest.utils.zeecommon.ui

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.salt.technicaltest.utils.zeecommon.CommonProvider
import com.salt.technicaltest.utils.zeecommon.extension.toast
import kotlin.reflect.KClass

abstract class AppDialogFragment<out T : ViewDataBinding, out V : AppViewModel>(resource: Int) :
    AppCompatDialogFragment() {

    protected abstract val viewModel: V

    protected val viewBinding: T by lazy {
        DataBindingUtil.inflate(layoutInflater, resource, null, false)
    }

    protected open val dialogWindowTransparent = false
    protected open val dialogWindowFullscreen = false
    protected open val dialogWindowGravity: Int? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            if (dialogWindowTransparent) window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialogWindowGravity?.let { window?.setGravity(it) }
        }
    }

    override fun onStart() {
        super.onStart()
        if (dialogWindowFullscreen) {
            dialog?.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.lifecycleOwner = viewLifecycleOwner

        // bindings
        if (this is HasViewModel) viewBinding.setVariable(brViewModelId, viewModel)
        else CommonProvider.getViewModelBindingId()
            ?.let { id -> viewBinding.setVariable(id, viewModel) }

        if (this is HasBindings) setupBinding()
        if (this is HasViewModel || this is HasBindings) viewBinding.executePendingBindings()

        // setup / preparation related
        if (this is HasViews) setupViews()
        if (this is HasObservers) setupObservers()

        setupDefaultObserver()
    }

    private fun setupDefaultObserver() {
        viewModel.showToastMessageEvent.observe(this) {
            requireActivity().toast(it)
        }
    }

    companion object {

        inline fun <A, B, reified T : AppDialogFragment<A, B>> AppCompatActivity.launchAppDialog(
            dialog: KClass<T>,
            block: T.() -> Unit = {}
        ) {
            (dialog.java.constructors.first().newInstance() as T).apply(block)
                .show(supportFragmentManager, T::class.java.canonicalName)
        }

        inline fun <A, B, reified T : AppDialogFragment<A, B>> Fragment.launchAppDialog(
            dialog: KClass<T>,
            block: T.() -> Unit = {}
        ) {
            (dialog.java.constructors.first().newInstance() as T).apply(block)
                .show(childFragmentManager, T::class.java.canonicalName)
        }

    }

}