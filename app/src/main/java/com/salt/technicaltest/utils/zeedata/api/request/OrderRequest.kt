package com.salt.technicaltest.utils.zeedata.api.request

import java.io.File

class OrderRequest (
    val luas: String,
    val deskripsi: String,
    val alamat: String,
    val jangka_waktu: String,
    val spek: String,
    val nama_lengkap: String,
    val telepon: String,
    val email: String,
    val kode_promo: String,
    val kode_referal: String,
    val metode_pembayaran: String,
    val type: String,
    val latitude: String,
    val longitude: String,

    //Addition By Desain
    var tipe_rumah: String = "",
    var ruang_keluarga: String = "",
    var kamar_tidur: String = "",
    var kamar_mandi: String = "",
    var dapur: String = ""
)