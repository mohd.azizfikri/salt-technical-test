package com.salt.technicaltest.utils.zeecommon.extension

import androidx.fragment.app.FragmentTransaction
import com.salt.technicaltest.R

fun FragmentTransaction.fadeVertical(): FragmentTransaction {
    setCustomAnimations(
        R.anim.fade_in_down,
        R.anim.fade_out_up,
        R.anim.fade_in_up,
        R.anim.fade_out_down
    )
    return this
}

fun FragmentTransaction.fadeHorizontal(): FragmentTransaction {
    setCustomAnimations(
        R.anim.fade_in_left,
        R.anim.fade_in_right,
        R.anim.fade_out_right,
        R.anim.fade_out_left
    )
    return this
}

fun FragmentTransaction.fade(): FragmentTransaction {
    setCustomAnimations(
        R.anim.fade_in,
        R.anim.fade_out,
        R.anim.fade_in,
        R.anim.fade_out
    )
    return this
}