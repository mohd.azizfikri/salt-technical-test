package com.salt.technicaltest.utils.zeedata.exception

class FailureRemoteResultException(message: String) : Exception(message)