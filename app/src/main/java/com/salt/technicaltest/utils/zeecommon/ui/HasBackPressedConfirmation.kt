package com.salt.technicaltest.utils.zeecommon.ui

interface HasBackPressedConfirmation {

    val backPressDelay: Long

}