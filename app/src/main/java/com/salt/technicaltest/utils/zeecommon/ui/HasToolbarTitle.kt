package com.salt.technicaltest.utils.zeecommon.ui

interface HasToolbarTitle {

    val toolbarTitle: String

}