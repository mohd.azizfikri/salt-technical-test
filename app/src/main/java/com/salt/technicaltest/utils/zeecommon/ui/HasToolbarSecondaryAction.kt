package com.salt.technicaltest.utils.zeecommon.ui

import android.view.View

interface HasToolbarSecondaryAction {

    val onToolbarSecondaryAction: (v: View) -> Unit

}