package com.salt.technicaltest.utils.zeedata.exception

class FailureResultException(message: String) : Exception(message)