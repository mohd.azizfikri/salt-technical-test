package com.salt.technicaltest.utils

object Constant {

    const val PREFIX_RP = "Rp "

    private val boolValMap = mapOf(true to "1", false to "0")
    private val boolStrMap = mapOf("1" to true, "0" to false)

    fun parseBool(strVal: String?) = boolStrMap[strVal ?: "0"] ?: false

    fun parseBool(boolVal: Boolean?) = boolValMap[boolVal ?: false]
}