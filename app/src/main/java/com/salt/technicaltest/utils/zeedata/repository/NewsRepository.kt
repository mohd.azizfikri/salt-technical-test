package com.salt.technicaltest.utils.zeedata.repository

import com.natpryce.*
import com.salt.technicaltest.utils.zeedata.api.response.itemresponse.NewsResponse
import com.salt.technicaltest.utils.zeedata.util.AppRepository

class NewsRepository : AppRepository() {

    suspend fun getEverything(
        keyword: String? = null,
        from: String? = null,
        to: String? = null,
        sortBy: String? = null
    ): Result<List<NewsResponse>, Exception> {
        val mKeyword = if(keyword.isNullOrEmpty()){
            "keyword"
        }else{
            keyword
        }
        return asResultList {
            getApi().newsEverything(
                mKeyword, from, to, sortBy
            )
        }
    }

    suspend fun getTopHeadlines(
        keyword: String? = null,
        country: String? = null,
        category: String? = null,
        sources: String? = null,
        pageSize: String? = null,
        page: String? = null
    ): Result<List<NewsResponse>, Exception> {
        val mCountry = if(keyword.isNullOrEmpty()){
            "id"
        }else{
            country
        }
        return asResultList {
            getApi().newsTopHeadlines(
                keyword, mCountry, category, sources, pageSize, page
            )
        }
    }

    suspend fun getBannerSilderList(): Result<List<NewsResponse>, Exception>{
        val result = asResultList { getApi().newsTopHeadlines( country = "id" ) }.onFailure {
            return it
        }
        return Success(result.subList(0, 4))
    }

}