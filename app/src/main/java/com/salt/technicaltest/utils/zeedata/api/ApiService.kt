package com.salt.technicaltest.utils.zeedata.api

import com.salt.technicaltest.utils.zeedata.api.response.general.DataListResponse
import com.salt.technicaltest.utils.zeedata.api.response.itemresponse.*
import com.salt.technicaltest.utils.zeedata.api.response.general.DataResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    /**
     * NEWS ===============================================================================
     */

    @GET("v2/everything")
    suspend fun newsEverything(
        @Query("q") keyword: String? = null,
        @Query("from") from: String? = null,
        @Query("to") to: String? = null,
        @Query("sortBy") sortBy: String? = null
    ): Response<DataListResponse<NewsResponse>>

    @GET("v2/top-headlines")
    suspend fun newsTopHeadlines(
        @Query("q") keyword: String? = null,
        @Query("country") country: String? = null,
        @Query("category") category: String? = null,
        @Query("sources") sources: String? = null,
        @Query("pageSize") pageSize: String? = null,
        @Query("page") page: String? = null
    ): Response<DataListResponse<NewsResponse>>


}