package com.salt.technicaltest.utils.zeedata

import com.salt.technicaltest.utils.zeedata.db.AppDatabase
import com.salt.technicaltest.utils.zeedata.prefs.AppPrefs

data class DataConfiguration(
    val remoteHost: String = "http:/8.215.26.192/",
    val remoteTimeout: Long = 30,
    val databaseName: String = AppDatabase.DEFAULT_DATABASE_NAME,
    val prefsName: String = AppPrefs.DEFAULT_PREFS_NAME,
    val prefsAlwaysCommit: Boolean = false,
    val debug: Boolean = true
)