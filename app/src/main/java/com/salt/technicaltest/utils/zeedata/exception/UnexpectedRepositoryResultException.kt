package com.salt.technicaltest.utils.zeedata.exception

class UnexpectedRepositoryResultException(message: String = "Unexpected error occured."): Exception(message)