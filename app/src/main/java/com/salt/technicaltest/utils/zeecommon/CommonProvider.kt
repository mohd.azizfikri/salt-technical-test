package com.salt.technicaltest.utils.zeecommon

object CommonProvider {

    private var mViewModelBindingId: Int? = null

    fun init(viewModelBindingId: Int) {
        mViewModelBindingId = viewModelBindingId
    }

    fun getViewModelBindingId() = mViewModelBindingId

}