package com.salt.technicaltest.utils.zeedata.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Configuration(
    @PrimaryKey val identifier: String = "",
    val content: String = ""
) {

    companion object {
        // any configuration keys
        const val KEY_TOKEN: String = "USER_TOKEN"
        const val KEY_USERNAME: String = "USERNAME"

        const val YES = "YES"
        const val NO = "NO"
    }

}