package com.salt.technicaltest.utils.zeedata.exception

class FailureDbResultException(message: String) : Exception(message)