package com.salt.technicaltest.utils.zeedata.exception.error

class IncompleteSignInInformationError(message: String? = null) : Exception(message)