package com.salt.technicaltest.di

import android.content.Context
import com.salt.technicaltest.utils.ResourceManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ApplicationComponent::class)
object CommonModule {

    @Provides
    fun resourceManager(@ApplicationContext context: Context): ResourceManager {
        return ResourceManager(context)
    }

}