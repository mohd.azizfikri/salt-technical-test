package com.salt.technicaltest.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ServiceComponent
import com.salt.technicaltest.utils.zeedata.repository.*

@Module
@InstallIn(ActivityComponent::class, ServiceComponent::class)
object RepositoryModule {

    @Provides
    fun provideNewsRepository() = NewsRepository()

}