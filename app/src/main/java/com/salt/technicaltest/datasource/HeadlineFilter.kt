package com.salt.technicaltest.datasource

class HeadlineFilter(
    val keyword: String? = null,
    val country: String? = null,
    val category: String? = null
)
