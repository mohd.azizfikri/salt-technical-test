package com.salt.technicaltest.datasource

import com.salt.technicaltest.common.adapter.SlidingBannerAdapter
import com.salt.technicaltest.utils.zeedata.api.response.itemresponse.NewsResponse

class Banner(
    val id: String,
    val imgUrl: String,
    val data: NewsResponse
) : SlidingBannerAdapter.Item(imgUrl) {

    companion object {

        fun map(banner: NewsResponse): Banner {
            return Banner(
                banner.url.orEmpty(),
                banner.urlToImage.orEmpty(),
                banner
            )
        }

    }
}