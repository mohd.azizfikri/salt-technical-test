package com.salt.technicaltest.feature.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.salt.technicaltest.utils.zeecommon.ui.AppViewModel

class SplashScreenViewModel @ViewModelInject constructor() : AppViewModel() {
    val showSubline by lazy { MutableLiveData(false) }

    fun changeSublineStatus(){
        showSubline.value = true
    }

}