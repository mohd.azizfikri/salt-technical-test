package com.salt.technicaltest.feature.main

import android.content.Intent
import androidx.activity.viewModels
import com.google.gson.Gson
import com.salt.technicaltest.R
import com.salt.technicaltest.common.adapter.ArticleListAdapter
import com.salt.technicaltest.databinding.ActivityMainBinding
import com.salt.technicaltest.feature.news.detail.NewsDetailActivity
import com.salt.technicaltest.feature.news.headline.HeadlineNewsActivity
import com.salt.technicaltest.utils.zeecommon.ui.AppActivity
import com.salt.technicaltest.utils.zeecommon.ui.HasObservers
import com.salt.technicaltest.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppActivity<ActivityMainBinding, MainViewModel>
    (R.layout.activity_main), HasViews, HasObservers {

    override val viewModel by viewModels<MainViewModel>()

    private val mArticleAdapter by lazy { ArticleListAdapter(viewModel) }

    override fun setupViews() {
        viewBinding.rvArtikel.adapter = mArticleAdapter

        viewBinding.ltoolbar.setOnThirdIconClick {
            startActivity(Intent(this, HeadlineNewsActivity::class.java))
        }
    }

    override fun setupObservers() {
        viewModel.openArticleDetailEvent.observe(this) {
            NewsDetailActivity.launch(this, Gson().toJson(it))
        }
    }

}