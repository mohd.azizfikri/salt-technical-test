package com.salt.technicaltest.feature.news.detail

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.google.gson.Gson
import com.salt.technicaltest.R
import com.salt.technicaltest.databinding.ActivityNewsDetailBinding
import com.salt.technicaltest.utils.zeecommon.ui.AppActivity
import com.salt.technicaltest.utils.zeecommon.ui.HasObservers
import com.salt.technicaltest.utils.zeecommon.ui.HasViews
import com.salt.technicaltest.utils.zeedata.api.response.itemresponse.NewsResponse
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsDetailActivity : AppActivity<ActivityNewsDetailBinding, NewsDetailViewModel>
    (R.layout.activity_news_detail), HasViews {

    override val viewModel by viewModels<NewsDetailViewModel>()

    private val mExtraContent by lazy { intent.getStringExtra(EXTRA_CONTENT) }

    override fun setupViews() {
        viewModel.setup(Gson().fromJson(mExtraContent, NewsResponse::class.java))
    }

    companion object {
        const val EXTRA_CONTENT = "EXTRA_CONTENT"
        fun launch(activity: Activity, content: String){
            activity.startActivity(Intent(activity, NewsDetailActivity::class.java).apply {
                putExtra(EXTRA_CONTENT, content)
            })
        }
    }
}