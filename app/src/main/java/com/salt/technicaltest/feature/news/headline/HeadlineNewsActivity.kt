package com.salt.technicaltest.feature.news.headline

import androidx.activity.viewModels
import com.google.gson.Gson
import com.salt.technicaltest.R
import com.salt.technicaltest.common.adapter.ArticleListAdapter
import com.salt.technicaltest.common.adapter.HomeBannerAdapter
import com.salt.technicaltest.common.dialog.FilterDialog
import com.salt.technicaltest.databinding.ActivityHeadlineNewsBinding
import com.salt.technicaltest.feature.news.detail.NewsDetailActivity
import com.salt.technicaltest.utils.zeecommon.ui.AppActivity
import com.salt.technicaltest.utils.zeecommon.ui.HasObservers
import com.salt.technicaltest.utils.zeecommon.ui.HasViews
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HeadlineNewsActivity : AppActivity<ActivityHeadlineNewsBinding, HeadlineNewsViewModel>
    (R.layout.activity_headline_news), HasViews, HasObservers {

    override val viewModel by viewModels<HeadlineNewsViewModel>()

    private val mBannerAdapter by lazy { HomeBannerAdapter(viewModel) }

    private val mArticleAdapter by lazy { ArticleListAdapter(viewModel) }

    override fun setupViews() {
        viewBinding.slidingBanner.init(mBannerAdapter)
        viewBinding.slidingBanner.setAutoScroll(true, 5000, this)

        viewBinding.rvArtikel.adapter = mArticleAdapter

        viewBinding.ltoolbar.setOnPrimaryIconClick {
            finish()
        }
    }

    override fun setupObservers() {
        viewModel.bannerList.liveData.observe(this) {
            viewBinding.slidingBanner.submitItems(it)
        }
        viewModel.openFilterEvent.observe(this) {
            FilterDialog {
                viewModel.searchText.value = validateString(it.keyword)
                viewModel.country.value = it.country ?: "id"
                viewModel.category.value = validateString(it.category)

                viewModel.filterNews()
            }.show(supportFragmentManager, FilterDialog::class.java.canonicalName)
        }
        viewModel.openArticleDetailEvent.observe(this) {
            NewsDetailActivity.launch(this, Gson().toJson(it))
        }
    }

    private fun validateString(data: String?): String? {
        return if(data == ""){
            null
        }else{
            data
        }
    }
}