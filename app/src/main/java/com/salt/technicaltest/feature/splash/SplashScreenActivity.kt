package com.salt.technicaltest.feature.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.activity.viewModels
import com.salt.technicaltest.R
import com.salt.technicaltest.databinding.ActivitySplashScreenBinding
import com.salt.technicaltest.feature.main.MainActivity
import com.salt.technicaltest.utils.zeecommon.ui.AppActivity
import com.salt.technicaltest.utils.zeecommon.ui.HasViews

class SplashScreenActivity : AppActivity<ActivitySplashScreenBinding, SplashScreenViewModel>
    (R.layout.activity_splash_screen), HasViews {
    override val viewModel by viewModels<SplashScreenViewModel>()

    override fun setupViews() {
        val timer = object: CountDownTimer(6000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (millisUntilFinished.toInt() == 4500){
                    viewModel.changeSublineStatus()
                }
            }

            override fun onFinish() {
                startActivity(Intent(applicationContext, MainActivity::class.java))
            }
        }
        timer.start()
    }
}