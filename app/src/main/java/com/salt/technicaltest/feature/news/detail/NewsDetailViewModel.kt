package com.salt.technicaltest.feature.news.detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.salt.technicaltest.utils.zeecommon.ui.AppViewModel
import com.salt.technicaltest.utils.zeedata.api.response.itemresponse.NewsResponse

class NewsDetailViewModel @ViewModelInject constructor() : AppViewModel() {

    val newsDetail by lazy { MutableLiveData<NewsResponse>() }

    fun setup(data: NewsResponse){
        newsDetail.value = data
    }

}