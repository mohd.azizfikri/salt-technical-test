package com.salt.technicaltest.feature.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.natpryce.Success
import com.natpryce.onFailure
import com.salt.technicaltest.common.adapter.ArticleListAdapter
import com.salt.technicaltest.utils.zeecommon.SelectableData
import com.salt.technicaltest.utils.zeecommon.extension.toFalse
import com.salt.technicaltest.utils.zeecommon.ui.AppViewModel
import com.salt.technicaltest.utils.zeecommon.util.event.LiveEvent
import com.salt.technicaltest.utils.zeecommon.util.event.MutableLiveEvent
import com.salt.technicaltest.utils.zeecommon.util.list.AppDataWrapper
import com.salt.technicaltest.utils.zeedata.api.response.itemresponse.NewsResponse
import com.salt.technicaltest.utils.zeedata.repository.NewsRepository
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val newsRepository: NewsRepository
) : AppViewModel(), ArticleListAdapter.ArticleListener {
    val articleListWrapper by lazy { AppDataWrapper<SelectableData<NewsResponse>>() }

    private val _openArticleDetailEvent by lazy { MutableLiveEvent<NewsResponse>() }
    val openArticleDetailEvent: LiveEvent<NewsResponse> = _openArticleDetailEvent

    val searchText by lazy { MutableLiveData<String>() }
    val fromDate by lazy { MutableLiveData<String>() }
    val toDate by lazy { MutableLiveData<String>() }
    val sortBy by lazy { MutableLiveData<String>() }

    init {
        fetchNews()
    }

    fun filterNews(){
        fetchNews(
            keyword = searchText.value,
            from = fromDate.value,
            to = toDate.value,
            sortBy = sortBy.value
        )
    }

    private fun fetchNews(
        keyword: String? = null,
        from: String? = null,
        to: String? = null,
        sortBy: String? = null
    ) {
        viewModelScope.launch {
            articleListWrapper.load {
                val results = newsRepository.getEverything(keyword, from, to, sortBy).onFailure {
                    loading.toFalse()
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    override fun onClickArticle(data: NewsResponse) {
        _openArticleDetailEvent.set(data)
    }
}