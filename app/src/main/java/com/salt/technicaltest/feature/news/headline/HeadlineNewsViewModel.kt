package com.salt.technicaltest.feature.news.headline

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.natpryce.Success
import com.natpryce.onFailure
import com.salt.technicaltest.common.adapter.ArticleListAdapter
import com.salt.technicaltest.common.adapter.HomeBannerAdapter
import com.salt.technicaltest.datasource.Banner
import com.salt.technicaltest.utils.zeecommon.SelectableData
import com.salt.technicaltest.utils.zeecommon.extension.toFalse
import com.salt.technicaltest.utils.zeecommon.ui.AppViewModel
import com.salt.technicaltest.utils.zeecommon.util.event.LiveEvent
import com.salt.technicaltest.utils.zeecommon.util.event.MutableLiveEvent
import com.salt.technicaltest.utils.zeecommon.util.list.AppDataWrapper
import com.salt.technicaltest.utils.zeecommon.util.list.AppDataWrapperWithMapper
import com.salt.technicaltest.utils.zeedata.api.response.itemresponse.NewsResponse
import com.salt.technicaltest.utils.zeedata.repository.NewsRepository
import kotlinx.coroutines.launch

class HeadlineNewsViewModel @ViewModelInject constructor(
    private val newsRepository: NewsRepository
) : AppViewModel(), HomeBannerAdapter.HomeBannerListener, ArticleListAdapter.ArticleListener {

    val articleListWrapper by lazy { AppDataWrapper<SelectableData<NewsResponse>>() }

    val bannerList by lazy { AppDataWrapperWithMapper(Banner::map) }

    private val _openArticleDetailEvent by lazy { MutableLiveEvent<NewsResponse>() }
    val openArticleDetailEvent: LiveEvent<NewsResponse> = _openArticleDetailEvent

    private val _openFilterEvent by lazy { MutableLiveEvent<Unit>() }
    val openFilterEvent: LiveEvent<Unit> = _openFilterEvent

    val searchText by lazy { MutableLiveData<String>() }
    val country by lazy { MutableLiveData<String>() }
    val category by lazy { MutableLiveData<String>() }
    val sources by lazy { MutableLiveData<String>() }
    val pageSize by lazy { MutableLiveData<String>() }
    val page by lazy { MutableLiveData<String>() }

    init {
        fetchBannerList()
        fetchNews()
    }

    fun filterNews(){
        fetchBannerList()
        fetchNews(
            keyword = searchText.value,
            country = country.value,
            category = category.value,
            sources = sources.value,
            pageSize = pageSize.value,
            page = page.value
        )
    }

    private fun fetchBannerList() {
        viewModelScope.launch {
            bannerList.load { newsRepository.getBannerSilderList() }
        }
    }

    private fun fetchNews(
        keyword: String? = null,
        country: String? = null,
        category: String? = null,
        sources: String? = null,
        pageSize: String? = null,
        page: String? = null
    ) {
        viewModelScope.launch {
            articleListWrapper.load {
                val results = newsRepository.getTopHeadlines(
                    keyword, country, category, sources, pageSize, page
                ).onFailure {
                    loading.toFalse()
                    return@load Success(emptyList())
                }
                loading.toFalse()
                Success(results.map { SelectableData(origin = it) })
            }
        }
    }

    fun onFilterClick(){
        _openFilterEvent.set(Unit)
    }

    override fun onClickArticle(data: NewsResponse) {
        _openArticleDetailEvent.set(data)
    }

    override fun onClick(item: Banner) {
        _openArticleDetailEvent.set(item.data)
    }
}